package agt

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"path/filepath"
	"slices"
	"sync"
	"time"
)

type CivilianAgent interface {
	Start()
	Percept(env *Env)
	Deliberate()
	Act(env *Env)
	ID() string
	Increment_chrono(string)
	Speak() []string
	Coords() Coordinates
	SetCoords(Coordinates)
}

type RebelAgent struct {
	id           string
	position     Coordinates
	Vocab        sync.Map
	inPrison     bool
	decision     Action
	goalCoords   Coordinates
	world        sync.Map
	objects      map[Coordinates]Objet
	visionRange  Coordinates
	cooldownTalk map[CivilianAgent]int
	env          *Env
	SyncChan     chan int
}

func randomInt(n int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(int(n))
}

func (ra *RebelAgent) Coords() Coordinates {
	return ra.position
}

func (ra *RebelAgent) SetCoords(coordinates Coordinates) {
	ra.position = coordinates
}

func (ra *RebelAgent) Increment_chrono(word string) {
	val, exist := ra.Vocab.Load(word)
	if exist {
		ra.Vocab.Store(word, val.(int)+1)
		fmt.Printf("Agent %s, mon chrono vient d'être incrémenté car j'ai entendu le mot %s \n", ra.id, word)
	} else {
		ra.Vocab.Store(word, 10)
		fmt.Printf("Agent %s, je viens d'apprendre le mot %s \n", ra.id, word)
	}
}

// la fonction retourne false si le mot est supprimé du vocabulaire de l'agent
// si false est retourné, u check doit être fait sur le vocabulaire
func (ra *RebelAgent) Decrement_chrono(word string) bool {
	timeLeft, _ := ra.Vocab.Load(word)
	ra.Vocab.Store(word, timeLeft.(int)-1)
	if timeLeft.(int) <= 1 {
		//ra.Delete_word_from_dico(ra.vocab, word)
		return false
	}
	return true
}

func (ra *RebelAgent) Decrement_all_chrono(env *Env) {
	//fmt.Printf("Agent %s decrement all chrono \n", ra.ID())
	ra.Vocab.Range(
		func(word, timeLeft any) bool {
			if timeLeft.(int) == 1 {
				ra.Vocab.Delete(word)
				fmt.Printf("Agent %s je suprime le mot %s \n", ra.ID(), word.(string))
				env.Check_Word(word.(string))

			} else {
				ra.Vocab.Store(word, timeLeft.(int)-1)
			}
			return true
		})
}

func (ra *RebelAgent) ID() string {
	return ra.id
}

func (ra *RebelAgent) initVocab() {
	absPath, _ := filepath.Abs("file_vocab/vocab.txt")
	vocabMap := Get_word_from_dico(absPath)
	for word, timeLimit := range vocabMap {
		ra.Vocab.Store(word, timeLimit)
	}
}

func NewRebelAgent(id string, pos Coordinates, environnement *Env) *RebelAgent {
	fmt.Printf("Création de l'agent {%s} \n", id)
	object := make(map[Coordinates]Objet)
	syncChan := make(chan int)

	ag := RebelAgent{
		inPrison:     false,
		id:           id,
		position:     pos,
		env:          environnement,
		SyncChan:     syncChan,
		objects:      object,
		visionRange:  Coordinates{2, 2},
		cooldownTalk: make(map[CivilianAgent]int),
	}
	ag.initVocab()
	return &ag
}

func (ra *RebelAgent) Start() {
	log.Printf("Agent %s starting...\n", ra.id)
	go func() {
		env := ra.env
		var step int
		for {
			step = <-ra.SyncChan
			//fmt.Printf("Agent {%s} tour n° : {%d} \n", ra.ID(), step)

			ra.Percept(env)
			ra.Deliberate()
			ra.Act(env)

			ra.SyncChan <- step
		}
	}()

}

func (ra *RebelAgent) Percept(env *Env) {
	env.Prison.Range(
		func(agent, _ any) bool {
			if agent.(CivilianAgent) == ra {
				ra.inPrison = true
				return false
			} else {
				ra.inPrison = false
			}
			return true
		})
	if !ra.inPrison {
		//fmt.Printf("Agent {%s} percept de l'environnement \n", ra.ID())
		ra.seeWithinRange(&env.World, ra.position, ra.visionRange)
		ra.objects = env.objects
	}
	for agt, cooldown := range ra.cooldownTalk {
		if cooldown > 0 {
			ra.cooldownTalk[agt]--
		} else {
			delete(ra.cooldownTalk, agt)
		}
	}
}

func distance(c1 Coordinates, c2 Coordinates) float64 {
	dx := float64(c2[0] - c1[0])
	dy := float64(c2[1] - c1[1])
	return math.Sqrt(dx*dx + dy*dy)
}

func moveFromCoords(ag Agent, emptyCells []Coordinates, goal Coordinates, closer bool) (Coordinates, bool) {
	distanceToGoal := make([]float64, 0)
	currentDistance := distance(ag.Coords(), goal)
	for _, coords := range emptyCells {
		distanceToGoal = append(
			distanceToGoal,
			distance(coords, goal),
		)
	}
	closestCoordinates := emptyCells[slices.Index(distanceToGoal, slices.Min(distanceToGoal))]
	closestDistance := distance(closestCoordinates, goal)
	if closer {
		if closestDistance <= currentDistance {
			return closestCoordinates, true
		}
	}
	if !closer {
		if closestDistance >= currentDistance {
			return closestCoordinates, true
		}
	}
	return Coordinates{0, 0}, false // on n'a pas trouvé de manière de se rapprocher
}

func (ra *RebelAgent) Deliberate() {
	//Recherche d'un agent rebelle voisin
	if ra.inPrison {
		ra.decision = Noop
		return
	}
	rand.Seed(time.Now().UnixNano())

	// Generate a random float between 0 (inclusive) and 1 (exclusive)
	randomFloat := rand.Float64()
	if randomFloat <= 0.05 {
		fmt.Printf("Agent %s decision new word \n", ra.id)
		ra.decision = New_word
		return
	}
	emptyCells := getNeighboringCells(ra.position)
	var canInteractCiv []Coordinates
	var seenCivilians []Coordinates
	var seenCops []Coordinates
	if ra.decision == Talk && len(emptyCells) > 0 {
		ra.decision = Move
		ra.goalCoords = emptyCells[rand.Intn(len(emptyCells))]
		return
	}
	ra.world.Range(
		func(tempCoords, agent any) bool {
			coords := tempCoords.(Coordinates)
			if coords != ra.position {
				switch agent.(type) {
				//Si elle contient un autre agent rebelle
				case CivilianAgent:
					if distance(ra.Coords(), agent.(Agent).Coords()) < 2 {
						canInteractCiv = append(canInteractCiv, coords)
						emptyCells = removeElement(emptyCells, coords)
					} else {
						seenCivilians = append(seenCivilians, coords)
					}
				case *CopAgent:
					seenCops = append(seenCops, coords)
				}
			}
			return true
		})
	// Ordre de priorités des Rebelles
	//S'il existe des agents rebelles voisins, il décide de parler avec l'un d'entre eux
	// 1: parler s'il va oublier un mot
	ra.Vocab.Range(
		func(_, timeLeft any) bool {
			if timeLeft.(int) < 10 && timeLeft.(int) > 1 {
				if nbInterCiv := len(canInteractCiv); nbInterCiv > 0 {
					ra.decision = Talk
					ra.goalCoords = canInteractCiv[rand.Intn(nbInterCiv)]
					return false
				} else { // aller vers un civil
					if nbSeenCiv := len(seenCivilians); nbSeenCiv > 0 && len(emptyCells) > 0 {
						if newCoords, found := moveFromCoords(ra, emptyCells, seenCivilians[rand.Intn(nbSeenCiv)], true); found {
							ra.decision = Move
							ra.goalCoords = newCoords
							return false
						}
					}
				}
			}
			return true
		})
	// 2: fuire un des policiers qu'il voit
	for _, coords := range seenCops {
		if len(emptyCells) > 0 {
			if newCoords, found := moveFromCoords(ra, emptyCells, coords, false); found {
				ra.decision = Move
				ra.goalCoords = newCoords
				return
			}
		}
	}
	// 3: parler à un voisin
	if nbCloseCiv := len(canInteractCiv); nbCloseCiv > 0 {
		talkCoords := canInteractCiv[rand.Intn(nbCloseCiv)]
		otherAgent, _ := ra.world.Load(talkCoords)
		if _, exists := ra.cooldownTalk[otherAgent.(CivilianAgent)]; !exists {
			ra.decision = Talk
			ra.goalCoords = canInteractCiv[rand.Intn(nbCloseCiv)]
			return
		}
	}
	// 5: bouger aléatoirement ?
	if rand.Float64() < 0.1 || len(emptyCells) == 0 {
		ra.decision = Noop
		ra.goalCoords = ra.position
		return
	} else {
		ra.decision = Move
		ra.goalCoords = emptyCells[rand.Intn(len(emptyCells))]
		return
	}
}

func (ra *RebelAgent) Act(env *Env) {
	//simulation d'une action
	switch ra.decision {
	case Noop:
		env.Do(ra.decision, Coordinates{0, 0}, ra)
	case Move:
		err := env.Do(ra.decision, ra.goalCoords, ra)
		if err == nil {
			ra.position = ra.goalCoords
			fmt.Printf("Agent {%s} je me suis déplacé en (%d, %d) \n", ra.ID(), ra.position[0], ra.position[1])
		}
	case Talk:
		err := env.Do(ra.decision, ra.goalCoords, ra)
		if err == nil {
			fmt.Printf("Agent {%s}, j'ai parlé \n", ra.ID())
			otherAgent, _ := ra.world.Load(ra.goalCoords)
			ra.cooldownTalk[otherAgent.(CivilianAgent)] = 20
		}
	case New_word:
		err := env.Do(ra.decision, Coordinates{0, 0}, ra)
		if err == nil {
		}

	default:
		panic("unhandled default case")
	}

}
