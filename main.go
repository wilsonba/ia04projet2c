package main

import (
	"fmt"
	"gitlab.utc.fr/wilsonba/ia04projet2c/simu"
)

const (
	MAXL = 10
	MAXH = 10
)

func main() {
	//initalisatin de la map
	//simu := simu.NewSimulation(2, 1, 10000, 600*time.Second, 10, 10)
	//simu.Run()

	server := simu.NewServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
