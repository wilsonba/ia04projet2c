# Babel Revolution - IA04 Groupe 2C

## Lancement de la simulation

`go run main.go` lance le serveur qui se met en attente d'une requête pour paramétrer et lancer la simulation.

Il faudra ensuite lancer le frontend associé avec `npm` ou directement envoyer des requêtes suivant les formats indiqués dans simu/server_side.go:

```
type CreationRequest struct {
	Height      int `json:"height"`
	Width       int `json:"width"`
	NbRebels    int `json:"nbRebels"`
	NbCops      int `json:"nbCops"`
	NbSpies     int `json:"nbspies"`
	MaxStep     int `json:"maxStep"`
	MaxDuration int `json:"maxDuration"`
}
```

