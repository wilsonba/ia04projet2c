package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func main() {

	for {
		fichier, err := os.Open("/Users/edouardblanc/Desktop/IA04/projet/ia04projet2c/file_vocab/vocab.txt")
		if err != nil {
			fmt.Println("Erreur lors de l'ouverture du fichier :", err)
		}
		defer fichier.Close()
		scanner := bufio.NewScanner(fichier)

		// Lire chaque ligne et extraire les mots
		nb_ligne := 0
		for scanner.Scan() {
			nb_ligne++

		}
		fmt.Println("nombre de ligne", nb_ligne)
		source := rand.NewSource(time.Now().UnixNano())
		generateur := rand.New(source)

		// Générer un entier aléatoire entre 1 et n
		entierAleatoire := generateur.Intn(nb_ligne) + 1
		fichier2, _ := os.Open("/Users/edouardblanc/Desktop/IA04/projet/ia04projet2c/file_vocab/vocab.txt")

		scanner2 := bufio.NewScanner(fichier2)
		ligneCourante := 0
		fmt.Printf("Entier choisir %d \n", entierAleatoire)

		for scanner2.Scan() {
			ligneCourante++
			fmt.Println("ligne courante", ligneCourante)
			if ligneCourante == entierAleatoire {
				mot := strings.Fields(scanner2.Text())
				fmt.Printf("Mot %s interdit par le comité", mot)
			}

		}
		time.Sleep(10 * time.Second)
	}

}
