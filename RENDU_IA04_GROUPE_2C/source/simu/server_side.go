package simu

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.utc.fr/wilsonba/ia04projet2c/agt"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

type StateRequest struct {
	Prison             []string                  `json:"prison"`
	World              map[string]string         `json:"world"`
	Sounds             map[string]map[string]int `json:"sounds"`
	NbWords            int                       `json:"nbWords"`
	ForbiddenWords     []string                  `json:"forbiddenWords"`
	Nb_mot_second      int                       `json:"nb_mot_second"`
	Nb_word_vocab      int                       `json:"nb_word_vocab"`
	Liste_agent_action map[string]agt.Action     `json:"liste_agent_action"`
}

type CreationRequest struct {
	Height      int `json:"height"`
	Width       int `json:"width"`
	NbRebels    int `json:"nbRebels"`
	NbCops      int `json:"nbCops"`
	NbSpies     int `json:"nbspies"`
	MaxStep     int `json:"maxStep"`
	MaxDuration int `json:"maxDuration"`
}

type ServerAgent struct {
	sync.Mutex
	id   string
	addr string
	simu *Simulation
}

func NewServerAgent(addr string) *ServerAgent {
	return &ServerAgent{id: addr, addr: addr}
}

// Test de la méthode
func (ssa *ServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func decodeRequest[Req CreationRequest](r *http.Request) (req Req, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (ssa *ServerAgent) sendState(w http.ResponseWriter, r *http.Request) {
	ssa.Lock()
	defer ssa.Unlock()

	if ssa.simu == nil {
		w.WriteHeader(http.StatusForbidden)
		msg := "No simulation running at the moment"
		w.Write([]byte(msg))
		return
	}

	// vérification de la méthode de la requête
	if !ssa.checkMethod("GET", w, r) {
		return
	}

	w.WriteHeader(http.StatusOK)

	absPath, _ := filepath.Abs("file_vocab/vocab.txt")
	vocab := agt.Get_word_from_dico(absPath)

	ssa.simu.env.Lock()

	seriSounds := make(map[string]map[string]int)
	ssa.simu.env.Sounds.Range(
		func(key, value any) bool {
			seriSounds[strconv.Itoa(key.(agt.Coordinates)[0])+","+strconv.Itoa(key.(agt.Coordinates)[1])] = value.(map[string]int)
			return true
		})

	seriWorld := make(map[string]string)
	ssa.simu.env.World.Range(
		func(key, value any) bool {
			if value != nil {
				seriWorld[strconv.Itoa(key.(agt.Coordinates)[0])+","+strconv.Itoa(key.(agt.Coordinates)[1])] = value.(agt.Agent).ID()
			}
			return true
		})

	seriPrison := make([]string, 0)
	ssa.simu.env.Prison.Range(
		func(agent, _ any) bool {
			seriPrison = append(seriPrison, agent.(agt.Agent).ID())
			return true
		})

	resp := StateRequest{
		seriPrison,
		seriWorld,
		seriSounds,
		len(vocab) - len(ssa.simu.env.ForbiddenWords),
		ssa.simu.env.ForbiddenWords,
		ssa.simu.env.NbWordsPerSec,
		ssa.simu.env.NbWordsVocab,
		ssa.simu.env.Liste_agent_action,
	}

	serial, err := json.Marshal(resp)
	ssa.simu.env.Unlock()

	fmt.Println(err)
	w.Write(serial)
}

func (ssa *ServerAgent) createNewSimu(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	ssa.Lock()
	defer ssa.Unlock()

	if ssa.simu != nil {
		w.WriteHeader(http.StatusForbidden)
		msg := "Simulation already running"
		w.Write([]byte(msg))
		return
	}

	// vérification de la méthode de la requête
	if !ssa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[CreationRequest](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	newSimu := NewSimulation(
		req.NbRebels,
		req.NbCops,
		req.NbSpies,
		req.MaxStep,
		time.Duration(req.MaxDuration*1e6),
		req.Width,
		req.Height,
	)
	ssa.simu = newSimu
	ssa.simu.Run()
}

func (ssa *ServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_simu", ssa.createNewSimu)
	mux.HandleFunc("/update", ssa.sendState)

	// Ajout du middleware CORS
	handler := CORSHandler(mux)

	// création du serveur http
	s := &http.Server{
		Addr:           ssa.addr,
		Handler:        handler, // Utilisation du gestionnaire avec le middleware CORS
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", ssa.addr)
	go log.Fatal(s.ListenAndServe())
}

// CORSHandler ajoute les en-têtes CORS à un gestionnaire HTTP
func CORSHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
		w.Header().Set("Access-Control-Allow-Credentials", "true")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		next.ServeHTTP(w, r)
	})
}
