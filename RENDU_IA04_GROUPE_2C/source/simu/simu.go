package simu

import (
	"bufio"
	"fmt"
	agt "gitlab.utc.fr/wilsonba/ia04projet2c/agt"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Simulation struct {
	sync.RWMutex
	url            string
	env            *agt.Env
	Agents         []agt.Agent
	maxStep        int
	maxDuration    time.Duration
	step           int // Stats
	start          time.Time
	syncChans      sync.Map
	nb_word_persec int
}

func nombreAleatoireEntreDeux(min, max int) int {
	// Initialiser la source de nombres aléatoires
	rand.Seed(time.Now().UnixNano())

	// Générer un entier aléatoire entre min (inclus) et max (exclus)
	nb := rand.Intn(max-min) + min
	return nb
}

func NewSimulation(nbRebelAgents int, nbCopAgents int, nbSpies int, maxStep int, maxDuration time.Duration, worldWidth int, worldHeight int) (simu *Simulation) {
	simu = &Simulation{}
	simu.Agents = make([]agt.Agent, 0, nbRebelAgents+nbCopAgents+nbSpies)
	simu.maxStep = maxStep
	simu.maxDuration = maxDuration

	//compte le nombre de ligne du dico
	fichier, _ := os.Open("file_vocab/vocab.txt")

	// Créer un scanner pour lire le fichier ligne par ligne
	scanner := bufio.NewScanner(fichier)

	// Compteur de lignes
	nombreDeLignes := 0

	// Lire le fichier ligne par ligne
	for scanner.Scan() {
		nombreDeLignes++
	}
	fichier.Close()
	simu.env = agt.NewEnvironment(worldWidth, worldHeight, nombreDeLignes)
	tableau := make(map[agt.Coordinates]bool, 0)
	// création des agents et des channels
	x := 0
	y := 0
	for i := 0; i < nbRebelAgents; i++ {
		// création de l'agent
		for {
			y = nombreAleatoireEntreDeux(0, simu.env.Get_MaxHeigth())
			x = nombreAleatoireEntreDeux(0, simu.env.Get_Maxlength())
			exist, _ := tableau[agt.Coordinates{x, y}]
			if exist {

			} else {
				break
			}

		}
		ag := agt.NewRebelAgent("RbAgt"+strconv.Itoa(i), agt.Coordinates{x, y}, simu.env)
		simu.Agents = append(simu.Agents, ag)
		simu.syncChans.Store(ag.ID(), ag.SyncChan)
		simu.env.Add_agent(x, y, ag)
		tableau[agt.Coordinates{x, y}] = true

	}

	// création des agents et des channels
	for i := 0; i < nbCopAgents; i++ {
		// création de l'agent
		for {
			y = nombreAleatoireEntreDeux(0, simu.env.Get_MaxHeigth())
			x = nombreAleatoireEntreDeux(0, simu.env.Get_Maxlength())
			exist, _ := tableau[agt.Coordinates{x, y}]
			if exist {

			} else {
				break
			}

		}
		ag := agt.NewCopAgent(
			"cop"+strconv.Itoa(i),
			simu.env,
			agt.Coordinates{x, y},
			agt.Coordinates{2, 2}, // vision range
			agt.Coordinates{3, 3}, // hearing range
		)
		simu.Agents = append(simu.Agents, ag)
		simu.syncChans.Store(ag.ID(), ag.SyncChan)
		simu.env.Add_agent(x, y, ag)
		tableau[agt.Coordinates{x, y}] = true

	}

	for i := 0; i < nbSpies; i++ {
		// création de l'agent
		for {
			y = nombreAleatoireEntreDeux(0, simu.env.Get_MaxHeigth())
			x = nombreAleatoireEntreDeux(0, simu.env.Get_Maxlength())
			exist, _ := tableau[agt.Coordinates{x, y}]
			if exist {

			} else {
				break
			}

		}
		ag := agt.NewSpyAgent(
			"SpyAgt"+strconv.Itoa(i),
			simu.env,
			agt.Coordinates{x, y},
			agt.Coordinates{2, 2}, // vision range
		)
		simu.Agents = append(simu.Agents, ag)
		simu.syncChans.Store(ag.ID(), ag.SyncChan)
		simu.env.Add_agent(x, y, ag)
		tableau[agt.Coordinates{x, y}] = true

	}

	return simu
}

func (simu *Simulation) Run() {
	for _, agent := range simu.Agents {
		agent.Start()
	}
	//mettre à jour tous les vocabulaires
	go func() {
		for {
			fmt.Printf("Simu je décrémente tout \n")
			time.Sleep(time.Second)
			action := agt.Decrement
			simu.env.Do(agt.Action(action), agt.Coordinates{}, nil)
		}
	}()

	go func() {
		entier_chosed := make(map[int]bool)
		for {
			absPath, _ := filepath.Abs("file_vocab/vocab.txt")
			fichier, err := os.Open(absPath)
			if err != nil {
				fmt.Println("Erreur lors de l'ouverture du fichier :", err)
			}
			defer fichier.Close()
			scanner := bufio.NewScanner(fichier)

			// Lire chaque ligne et extraire les mots
			nb_ligne := 0
			for scanner.Scan() {
				nb_ligne++

			}
			source := rand.NewSource(time.Now().UnixNano())
			generateur := rand.New(source)

			// Générer un entier aléatoire entre 1 et n
			entierAleatoire := generateur.Intn(nb_ligne) + 1
			for {
				_, exist := entier_chosed[entierAleatoire]
				if exist {
					continue
				}
				entier_chosed[entierAleatoire] = true
				break

			}

			fichier2, _ := os.Open(absPath)
			scanner2 := bufio.NewScanner(fichier2)

			ligneCourante := 0
			for scanner2.Scan() {
				ligneCourante++
				if ligneCourante == entierAleatoire {
					simu.env.ForbiddenWords = append(simu.env.ForbiddenWords, strings.Fields(scanner2.Text())...)
					mot := strings.Fields(scanner2.Text())
					fmt.Printf("Mot %s interdit par le comité\n", mot)
				}

			}
			time.Sleep(10 * time.Second)
		}
	}()

	// On sauvegarde la date du début de la simulation
	simu.start = time.Now()
	go func() {
		for {
			simu.env.Print()
			simu.env.Print_Sounds()
			time.Sleep(8 * time.Second)
		}

	}()
	//compter le nombre de mots par seconde
	go func() {
		for {
			temp_back := simu.env.Get_count()
			time.Sleep(1 * time.Second)
			temp_for := simu.env.Get_count()
			simu.env.NbWordsPerSec = temp_for - temp_back
			fmt.Printf("Nombre de mots dits par seconde: %d \n", simu.env.NbWordsPerSec)
		}
	}()

	//mettre à jour le nombre de mot du vocab

	for _, agent := range simu.Agents {
		go func(agent agt.Agent) {
			step := 0
			if simu.maxStep < 0 {
				for {
					step++
					c, _ := simu.syncChans.Load(agent.ID())
					c.(chan int) <- step
					time.Sleep(500 * time.Millisecond)
					<-c.(chan int)
				}
			} else {
				for step < simu.maxStep {
					step++
					c, _ := simu.syncChans.Load(agent.ID())
					c.(chan int) <- step
					time.Sleep(500 * time.Millisecond)
					<-c.(chan int)
				}
			}
		}(agent)
	}

	time.Sleep(simu.maxDuration)

}
