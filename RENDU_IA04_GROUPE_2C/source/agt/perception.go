package agt

import "sync"

func (ca *CopAgent) seeWithinRange(source *sync.Map, currentPosition Coordinates, perceptionRange Coordinates) {
	ca.world = sync.Map{} // clear cop's vision (technically should only clear out of range spots)
	for x := currentPosition[0] - perceptionRange[0]; x <= currentPosition[0]+perceptionRange[0]; x++ {
		for y := currentPosition[1] - perceptionRange[1]; y <= currentPosition[1]+perceptionRange[1]; y++ {
			coords := Coordinates{x, y}
			object, exists := source.Load(coords)
			if exists {
				ca.world.Store(coords, object)
			}
		}
	}
}

func (ca *CopAgent) hearWithinRange(source *sync.Map, currentPosition Coordinates, perceptionRange Coordinates) {
	ca.sounds = sync.Map{} // clear cop's vision (technically should only clear out of range spots)
	for x := currentPosition[0] - perceptionRange[0]; x <= currentPosition[0]+perceptionRange[0]; x++ {
		for y := currentPosition[1] - perceptionRange[1]; y <= currentPosition[1]+perceptionRange[1]; y++ {
			coords := Coordinates{x, y}
			object, exists := source.Load(coords)
			if exists {
				ca.sounds.Store(coords, object)
			}
		}
	}
}

func (ra *RebelAgent) seeWithinRange(source *sync.Map, currentPosition Coordinates, perceptionRange Coordinates) {
	ra.world = sync.Map{} // clear cop's vision (technically should only clear out of range spots)
	for x := currentPosition[0] - perceptionRange[0]; x <= currentPosition[0]+perceptionRange[0]; x++ {
		for y := currentPosition[1] - perceptionRange[1]; y <= currentPosition[1]+perceptionRange[1]; y++ {
			coords := Coordinates{x, y}
			object, exists := source.Load(coords)
			if exists {
				ra.world.Store(coords, object)
			}
		}
	}
}

func (spy *SpyAgent) seeWithinRange(source *sync.Map, currentPosition Coordinates, perceptionRange Coordinates) {
	spy.world = sync.Map{} // clear cop's vision (technically should only clear out of range spots)
	for x := currentPosition[0] - perceptionRange[0]; x <= currentPosition[0]+perceptionRange[0]; x++ {
		for y := currentPosition[1] - perceptionRange[1]; y <= currentPosition[1]+perceptionRange[1]; y++ {
			coords := Coordinates{x, y}
			object, exists := source.Load(coords)
			if exists {
				spy.world.Store(coords, object)
			}
		}
	}
}
