package agt

import (
	"fmt"
	"log"
	"math/rand"
	"strings"
	"sync"
)

type SpyAgent struct { // TODO: Entendre comme les rebelles, peut se faire emprisonner
	id             string
	position       Coordinates
	visionRange    Coordinates
	world          sync.Map
	objects        map[Coordinates]Objet
	cooldownCop    map[*CopAgent]int
	forbiddenWords []string
	suspects       []string
	decision       Action
	goalCoords     Coordinates
	inPrison       bool
	SyncChan       chan int
	env            *Env
}

func NewSpyAgent(id string, env *Env, pos, visionRange Coordinates) *SpyAgent {
	return &SpyAgent{
		id,
		pos,
		visionRange,
		sync.Map{},
		make(map[Coordinates]Objet),
		make(map[*CopAgent]int),
		make([]string, 0),
		make([]string, 0),
		Noop,
		Coordinates{},
		false,
		make(chan int),
		env,
	}
}

func (spy *SpyAgent) ID() string {
	return spy.id
}

func (spy *SpyAgent) Coords() Coordinates {
	return spy.position
}

func (spy *SpyAgent) Suspects() []string {
	return spy.suspects
}

func (spy *SpyAgent) SetCoords(coordinates Coordinates) {
	spy.position = coordinates
}

func (spy *SpyAgent) ReceiveSuspects(suspects []string) {
	for _, suspect := range suspects {
		if !contains(spy.suspects, suspect) {
			spy.suspects = append(spy.suspects, suspect)
		}
	}
}

func (spy *SpyAgent) Start() {
	log.Printf("%s starting...\n", spy.id)

	go func() {
		env := spy.env
		var step int
		for {
			step = <-spy.SyncChan

			spy.Percept(env)
			spy.Deliberate()
			spy.Act(env)

			spy.SyncChan <- step
		}
	}()
}

func (spy *SpyAgent) Increment_chrono(word string) {
	if contains(spy.forbiddenWords, word) {
		surroundCoords := getNeighboringCells(spy.position)
		for _, coords := range surroundCoords {
			if coords != spy.position {
				ag, _ := spy.world.Load(coords)
				switch ag.(type) {
				//Si elle contient un autre agent rebelle
				case CivilianAgent:
					spy.suspects = append(spy.suspects, ag.(Agent).ID())
				default:
				}
			}
		}
	}
}

func (spy *SpyAgent) Speak() []string { return nil }

func (spy *SpyAgent) Percept(env *Env) {
	env.Prison.Range(
		func(agent, _ any) bool {
			if agent.(CivilianAgent) == spy {
				spy.inPrison = true
				return false
			} else {
				spy.inPrison = false
			}
			return true
		})
	for agt, cooldown := range spy.cooldownCop {
		if cooldown > 0 {
			spy.cooldownCop[agt]--
		} else {
			delete(spy.cooldownCop, agt)
		}
	}
	spy.seeWithinRange(&env.World, spy.position, spy.visionRange)
	spy.objects = env.objects
	spy.forbiddenWords = env.ForbiddenWords
}

func (spy *SpyAgent) Deliberate() {
	if spy.inPrison {
		spy.decision = Noop
		return
	}
	var canInteractCop []Coordinates
	var seenCops []Coordinates
	var seenCivilians []Coordinates
	emptyCells := getNeighboringCells(spy.position)
	spy.world.Range(
		func(tempCoords, agent any) bool {
			coords := tempCoords.(Coordinates)
			if coords != spy.position {
				switch agent.(type) {
				//Si elle contient un autre agent rebelle
				case CivilianAgent:
					seenCivilians = append(seenCivilians, coords)
				case *CopAgent:
					if distance(spy.Coords(), agent.(Agent).Coords()) < 2 {
						canInteractCop = append(canInteractCop, coords)
						emptyCells = removeElement(emptyCells, coords)
					} else {
						seenCops = append(seenCops, coords)
					}
				}
			}
			return true
		})
	if len(spy.suspects) > 0 {
		for _, coords := range canInteractCop { // share suspects to cop
			// si on ne lui a pas parlé récemment, on peut lui partager notre liste de suspects
			ag, _ := spy.world.Load(coords)
			if _, exists := spy.cooldownCop[ag.(*CopAgent)]; !exists {
				spy.decision = ShareSuspects
				spy.goalCoords = coords
				return
			}
		}
	}
	if len(emptyCells) == 0 {
		spy.decision = Noop
		spy.goalCoords = spy.position
		return
	}
	if rand.Float64() <= .3 {
		if nbCops := len(seenCops); nbCops > 0 {
			if newCoords, found := moveFromCoords(spy, emptyCells, seenCops[rand.Intn(nbCops)], true); found {
				spy.decision = Move
				spy.goalCoords = newCoords
				return
			}
		}
	} else {
		if nbCivilians := len(seenCivilians); nbCivilians > 0 {
			if newCoords, found := moveFromCoords(spy, emptyCells, seenCivilians[rand.Intn(nbCivilians)], true); found {
				spy.decision = Move
				spy.goalCoords = newCoords
				return
			}
		}
	}
	// bouger aléatoirement
	if rand.Float64() < 0.1 || len(emptyCells) == 0 {
		spy.decision = Noop
		spy.goalCoords = spy.position
		return
	} else {
		spy.decision = Move
		spy.goalCoords = emptyCells[rand.Intn(len(emptyCells))]
		return
	}
}

func printList(list []string) string {
	numItems := len(list)
	var prefixe string
	if numItems == 1 {
		prefixe = "l'agent"
	} else {
		prefixe = "les agents"
	}
	resultat := fmt.Sprintf("%s %s", prefixe, strings.Join(list, ", "))
	return resultat
}

func (spy *SpyAgent) Act(env *Env) {
	//simulation d'une action
	//time.Sleep(500 * time.Millisecond)
	err := env.Do(spy.decision, spy.goalCoords, spy)
	switch spy.decision {
	case Noop:
	case Move:
		if err == nil {
			spy.position = spy.goalCoords
			fmt.Printf("Agent {%s} -> (%d, %d) \n", spy.ID(), spy.position[0], spy.position[1])
		}
	case ShareSuspects:
		if err == nil {
			ag, _ := spy.world.Load(spy.goalCoords)
			fmt.Printf("J'ai partagé mes suspects avec {%s} \n", ag.(Agent).ID())
			spy.cooldownCop[ag.(*CopAgent)] = 10
			spy.suspects = make([]string, 0)
		}
	default:
		fmt.Printf("L'action n°%d n'est pas implémentée, je ne sais pas ce que j'ai fait", spy.decision)
	}
}
