package agt

import (
	"bufio"
	"fmt"
	"math"
	"math/rand"
	"os"
	"strings"
	"time"
)

// fonction pour parler, ajouter un mot dans l'environnement
func (ra *RebelAgent) Speak() []string {

	var minCle string
	var minValeur int
	premier := true
	rand.Seed(time.Now().UnixNano())

	nombreAleatoire := rand.Intn(10) + 1
	var words_speak []string
	temp_voca := make(map[string]int)
	ra.Vocab.Range(
		func(word, timeLimit any) bool {
			temp_voca[word.(string)] = timeLimit.(int)
			return true
		})
	for i := 0; i < nombreAleatoire; i++ {
		for cle, valeur := range temp_voca {
			minValeur = math.MaxInt
			premier = true
			if premier || valeur < minValeur {
				minCle = cle
				minValeur = valeur
				premier = false
			}
		}
		words_speak = append(words_speak, minCle)
		delete(temp_voca, minCle)
	}

	return words_speak
}

func (ra *RebelAgent) Delete_word_from_dico(file_name string, mot string, agt_list []*RebelAgent) {
	//on vérifie que le mot n'est pas présent dans le vocabulaire d'autres agents
	for _, agt := range agt_list {
		_, present := agt.Vocab.Load(mot)
		if present == true {
			return
		}
	}
	//ouvrir un descripteur
	file, _ := os.OpenFile(file_name, os.O_RDWR, 0666)
	//fermer le descripteur
	defer file.Close()
	// ouvrir un nouveau buffer sur le fichier
	scanner := bufio.NewScanner(file)
	var lignes []string
	for scanner.Scan() {
		// Vérifier si la ligne contient le mot à supprimer
		if !strings.Contains(scanner.Text(), mot) {
			// Si non, ajouter la ligne à la tranche
			lignes = append(lignes, scanner.Text())
		}
	}

	// on vide le fichier pour pouvoir réecrire dessus
	file.Truncate(0)
	//on revient on début du fichier
	file.Seek(0, 0)
	//on créer un writer
	writer := bufio.NewWriter(file)

	//on réecrit toutes les lignes qui contenaient le mot
	for _, ligne := range lignes {
		fmt.Println("ligne  ", ligne)
		fmt.Fprintln(writer, ligne)
		writer.Flush()
	}

}

func write_word_dico(file_name string, mot string) {
	// Ouvrir le fichier en écriture
	fichier, _ := os.OpenFile(file_name, os.O_WRONLY|os.O_CREATE, 0666)
	writer := bufio.NewWriter(fichier)
	fmt.Fprintln(writer, mot)
	writer.Flush()
}

func Get_word_from_dico(file_name string) map[string]int {
	fichier, err := os.Open(file_name)
	vocab := make(map[string]int)
	if err != nil {
		fmt.Println("Erreur lors de l'ouverture du fichier :", err)
	}
	defer fichier.Close()
	scanner := bufio.NewScanner(fichier)

	// Lire chaque ligne et extraire les mots
	for scanner.Scan() {
		ligne := scanner.Text()
		mots := strings.Fields(ligne)

		// Traiter chaque mot
		for _, mot := range mots {
			vocab[mot] = 60
		}
	}
	return vocab
}
