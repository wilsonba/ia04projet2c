package agt

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Coordinates [2]int

func (c *Coordinates) Add(c2 Coordinates) Coordinates {
	return Coordinates{c[0] + c2[0], c[1] + c2[1]}
}

type Action int64

const (
	Noop = iota
	Move
	Talk
	Arrest
	EnterHouse // besoin d'un mot de passe ?
	LeaveHouse
	Hear
	Decrement
	ForbidWord
	ShareSuspects
	New_word
	Chrono_word int = 60
)

type Env struct {
	sync.RWMutex
	Prison         sync.Map
	maxlength      int
	maxheigth      int
	ForbiddenWords []string
	World          sync.Map
	Sounds         sync.Map
	objects        map[Coordinates]Objet
	NbWordsPerSec  int
	//compte le nombre total de mot dit
	Count              int
	NbWordsVocab       int
	Liste_agent_action map[string]Action
	lost_words         []string
}

func NewEnvironment(maxLength int, maxHeigth int, nb_word int) *Env {
	fmt.Println("Création de l'environnement")
	return &Env{
		maxlength:          maxLength,
		maxheigth:          maxHeigth,
		objects:            make(map[Coordinates]Objet),
		ForbiddenWords:     make([]string, 0),
		NbWordsVocab:       nb_word,
		Liste_agent_action: make(map[string]Action),
		lost_words:         make([]string, 0),
	}
}

func (environment *Env) Get_Maxlength() int {
	return environment.maxlength
}
func (environment *Env) Get_MaxHeigth() int {
	return environment.maxheigth
}
func (environment *Env) Add_agent(x int, y int, objet Agent) {
	key := Coordinates{x, y}
	_, exist := environment.World.Load(key)
	if exist {
		print("Je ne peux pas ajouter l'agent à cette case car la case est occupée")
		return
	}

	fmt.Printf("Ajout de l'agent {%s} \n", objet.ID())
	environment.World.Store(key, objet)
	return
}

// ajouter un mot sur la carte des sons
func (environment *Env) Add_word(c Coordinates, word string) {
	temp, exists := environment.Sounds.LoadOrStore(c, map[string]int{word: 1})
	if exists {
		//si le mot est déjà sur la case, on réincrémente son chrono
		if val, exist := temp.(map[string]int)[word]; exist {
			val += 1
			fmt.Printf("Envrionnement : mot {%s}, incrémenté à la case {%d, %d} \n ", word, c[0], c[1])
			return
		}
	}

	fmt.Printf("Envrionnement : mot {%s}, ajouté à la case {%d, %d}  \n", word, c[0], c[1])
}

// fonction pour récupérer la map de mot d'un case
func (environment *Env) Get_map_word_coor(c Coordinates) map[string]int {
	temp, _ := environment.Sounds.Load(c)
	return temp.(map[string]int)
}

func (environment *Env) Get_count() int {
	environment.Lock()
	defer environment.Unlock()
	return environment.Count
}
func (environment *Env) Do(a Action, c Coordinates, agent Agent) (err error) {
	environment.Lock()
	defer environment.Unlock()
	if agent != nil {
		environment.Liste_agent_action[agent.ID()] = a
	}

	switch agent.(type) {
	case CivilianAgent:
		if _, inPrison := environment.Prison.Load(agent.(CivilianAgent)); inPrison {
			fmt.Printf("Agent %s is in prison and cannot act\n", agent.ID())
			return fmt.Errorf("cannot act in prison")
		}
	}

	switch a {
	case Arrest:
		if _, exists := environment.World.Load(c); !exists {
			return fmt.Errorf("bad coordinates (%d, %d)", c[0], c[1])
		}
		environment.sendToPrison(c)
	case Move:
		if _, exists := environment.World.Load(c); exists {
			return fmt.Errorf("space already occupied (%d, %d)", c[0], c[1])
		}
		if (c[0] < 0 || c[0] >= environment.maxlength) || (c[1] < 0 || c[1] >= environment.maxheigth) {
			return fmt.Errorf("out of bounds (%d, %d)", c[0], c[1])
		}
		environment.World.Delete(agent.Coords())
		environment.World.Store(c, agent)
		return nil
		// plusieurs actions MoveUp, MoveDown... avec coordonnées de l'agent ? ou autrement ?
	case Talk:
		otherAgent, exists := environment.World.Load(c)
		if !exists {
			return fmt.Errorf("cannot talk to agent in (%d, %d)", c[0], c[1])
		}
		ag := agent.(CivilianAgent)
		word := ag.Speak()
		for _, mot := range word {
			ag.Increment_chrono(mot)
			environment.Count++
			environment.Add_word(ag.Coords(), mot)
			environment.Add_word(c, mot)
			switch otherAgent.(type) {
			case *CopAgent:
				return fmt.Errorf("agent in (%d, %d) is a cop", c[0], c[1])
			case CivilianAgent:
				agentTo := otherAgent.(CivilianAgent)
				agentTo.Increment_chrono(mot)
			}
		}
		return nil
	case Hear:

	case ForbidWord:

	case EnterHouse:

	case LeaveHouse:

	case ShareSuspects:
		switch agent.(type) {
		case *CopAgent:
			if otherAgent, exists := environment.World.Load(c); exists {
				switch otherAgent.(type) {
				case *CopAgent:
					otherAgent.(*CopAgent).ReceiveSuspects(agent.(*CopAgent).Suspects())
				default:
					return fmt.Errorf("agent %s in (%d, %d) is not a cop", otherAgent.(Agent).ID(), c[0], c[1])
				}
			}
		case *SpyAgent:
			if otherAgent, exists := environment.World.Load(c); exists {
				switch otherAgent.(type) {
				case *CopAgent:
					otherAgent.(*CopAgent).ReceiveSuspects(agent.(*SpyAgent).Suspects())
				default:
					return fmt.Errorf("agent %s in (%d, %d) is not a cop", otherAgent.(Agent).ID(), c[0], c[1])
				}
			}
		default:
			return fmt.Errorf("agent %s is not a cop and cannot share suspects", agent.ID())
		}
		return nil
	case Noop:
		return nil
	case New_word:
		sizeTemp := len(environment.lost_words)
		if sizeTemp == 0 {
			return fmt.Errorf("action impossible pour le moment")
		}
		rand.Seed(time.Now().UnixNano())
		ind := rand.Intn(sizeTemp)
		agent.(*RebelAgent).Increment_chrono(environment.lost_words[ind])
		environment.NbWordsVocab++
		fmt.Printf("Le mot %s a été appris de nouveau \n", environment.lost_words[ind])
		environment.lost_words = append(environment.lost_words[:ind], environment.lost_words[ind+1:]...)
	case Decrement:
		environment.Decrement_everything()

	}

	return fmt.Errorf("bad action number %d", a)
}

func (environment *Env) sendToPrison(c Coordinates) {
	key, _ := environment.World.Load(c)
	agent := key.(CivilianAgent)
	environment.Prison.Store(agent, 20)
	environment.World.Delete(c)
	fmt.Printf("Agent {%s} a été mis en prison \n", agent.ID())
}

func (environment *Env) PlaceOnFreeSpot(ag Agent) error {
	newCoords := Coordinates{0, 0}
	for {
		newCoords = Coordinates{rand.Intn(environment.maxlength), rand.Intn(environment.maxheigth)}

		if agent, exists := environment.World.Load(newCoords); !exists || agent == nil {
			break
		}
	}

	ag.SetCoords(newCoords)
	environment.World.Store(newCoords, ag)
	fmt.Printf("agent %s has been placed on (%d, %d)\n", ag.ID(), newCoords[0], newCoords[1])
	return nil
}

func (environment *Env) Decrement_everything() {
	//décrémente tous les mots
	environment.Sounds.Range(
		func(tempCoords, tempWords any) bool {
			coords := tempCoords.(Coordinates)
			words := tempWords.(map[string]int)
			for word, _ := range words {
				if words[word] == 1 {
					delete(words, word)
					fmt.Printf("Simu: Le mot %s disparait de la case [%d,%d]\n", word, coords[0], coords[1])
				} else {
					words[word]--
				}
			}
			return true
		})

	//décrémente tous les agents
	environment.World.Range(
		func(_, agent any) bool {
			switch agent.(type) {
			case *RebelAgent:
				agent.(*RebelAgent).Decrement_all_chrono(environment)
			}
			return true
		})

	environment.Prison.Range(
		func(agent, _ any) bool {
			switch agent.(type) {
			case *RebelAgent:
				agent.(*RebelAgent).Decrement_all_chrono(environment)
			}
			return true
		})

	// décrémente la sentence de prison
	environment.Prison.Range(
		func(agent, secondsLeft any) bool {
			if secondsLeft == 1 {
				ag := agent.(CivilianAgent)
				environment.Prison.Delete(agent)
				err := environment.PlaceOnFreeSpot(ag)
				if err != nil {
					panic(err) // c'est vraiment pas normal si l'environnement est plein
				}
				fmt.Printf("l'agent %s a été libéré\n", ag.ID())
			} else {
				environment.Prison.Swap(agent, secondsLeft.(int)-1)
			}
			return true
		})
}

func (environment *Env) Print() {
	// Afficher la première ligne de délimitation supérieure
	environment.Lock()
	defer environment.Unlock()
	fmt.Print("+-")
	for k := 0; k < environment.maxlength; k++ {
		fmt.Print("----+")
	}
	fmt.Println()

	// Afficher le contenu de la grille avec les délimitations
	for i := 0; i < environment.maxlength; i++ {
		fmt.Print("| ")
		for j := 0; j < environment.maxheigth; j++ {
			key := Coordinates{i, j}
			agent, exist := environment.World.Load(key)
			if exist && agent != nil {
				fmt.Printf(" %s|", padString(agent.(Agent).ID(), 8))
			} else {
				fmt.Print("    |")
			}
		}
		fmt.Println()
		// Afficher une ligne de délimitation entre chaque ligne
		fmt.Print("+-")
		for k := 0; k < environment.maxlength; k++ {
			fmt.Print("----+")
		}
		fmt.Println()
	}
}

func (environment *Env) Print_Sounds() {
	// Afficher la première ligne de délimitation supérieure
	environment.Lock()
	defer environment.Unlock()
	fmt.Print("+-")
	for k := 0; k < environment.maxlength; k++ {
		fmt.Print("----+")
	}
	fmt.Println()

	// Afficher le contenu de la grille avec les délimitations
	for i := 0; i < environment.maxlength; i++ {
		fmt.Print("| ")
		for j := 0; j < environment.maxheigth; j++ {
			key := Coordinates{i, j}
			wordMap, exist := environment.Sounds.Load(key)

			if exist {
				fmt.Printf("Nbmots: %s|", padString(strconv.Itoa(len(wordMap.(map[string]int))), 4))
			} else {
				fmt.Print("    |")
			}
		}
		fmt.Println()
		// Afficher une ligne de délimitation entre chaque ligne
		fmt.Print("+-")
		for k := 0; k < environment.maxlength; k++ {
			fmt.Print("----+")
		}
		fmt.Println()
	}
}

func (environment *Env) Check_Word(word string) {
	check := false
	environment.World.Range(
		func(_, value any) bool {
			switch value.(type) {
			case *RebelAgent:
				agent := value.(*RebelAgent)
				_, check = agent.Vocab.Load(word)
			}
			if check == true {
				return false
			}
			return true
		})

	if check == false {
		if environment.NbWordsVocab <= 0 {
			fmt.Println("Les agents ont perdu leur langue")
			return
		}
		environment.NbWordsVocab--
		fmt.Printf("Environnement, suppression de %s du vocabulaire des agents \n", word)
		environment.lost_words = append(environment.lost_words, word)
	}
}

func padString(input string, cellSize int) string {
	if len(input) >= cellSize {
		return input
	}
	numSpaces := cellSize - len(input) - 1
	paddedString := input + strings.Repeat(" ", numSpaces)
	return paddedString
}
