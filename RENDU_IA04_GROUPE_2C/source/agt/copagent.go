package agt

import "C"
import (
	"fmt"
	"log"
	"math/rand"
	"sync"
)

type Objet interface {
	ID() string
}

type Agent interface {
	Start()
	Percept(env *Env)
	Deliberate()
	Act(env *Env)
	SetCoords(Coordinates)
	Coords() Coordinates
	ID() string
}

type CopAgent struct {
	id             string
	position       Coordinates
	visionRange    Coordinates
	hearingRange   Coordinates
	world          sync.Map
	sounds         sync.Map
	objects        map[Coordinates]Objet
	cooldownCop    map[*CopAgent]int
	forbiddenWords []string
	suspects       []string
	decision       Action
	goalCoords     Coordinates
	SyncChan       chan int
	env            *Env
}

func NewCopAgent(id string, env *Env, pos, visionRange, hearingRange Coordinates) *CopAgent {
	return &CopAgent{
		id,
		pos,
		visionRange,
		hearingRange,
		sync.Map{},
		sync.Map{},
		make(map[Coordinates]Objet),
		make(map[*CopAgent]int),
		make([]string, 0),
		make([]string, 0),
		Noop,
		Coordinates{},
		make(chan int),
		env,
	}
}

func (ca *CopAgent) ID() string {
	return ca.id
}

func (ca *CopAgent) Coords() Coordinates {
	return ca.position
}

func (ca *CopAgent) SetCoords(coordinates Coordinates) {
	ca.position = coordinates
}

func (ca *CopAgent) Suspects() []string {
	return ca.suspects
}

func (ca *CopAgent) ReceiveSuspects(suspects []string) {
	for _, suspect := range suspects {
		if !contains(ca.suspects, suspect) {
			ca.suspects = append(ca.suspects, suspect)
		}
	}
}

func (ca *CopAgent) Start() {
	log.Printf("%s starting...\n", ca.id)

	go func() {
		env := ca.env
		var step int
		for {
			step = <-ca.SyncChan

			ca.Percept(env)
			ca.Deliberate()
			ca.Act(env)

			ca.SyncChan <- step
		}
	}()
}

func (ca *CopAgent) Percept(env *Env) {
	// à compléter avec percept de Gabrielle
	ca.seeWithinRange(&env.World, ca.position, ca.visionRange)
	ca.hearWithinRange(&env.Sounds, ca.position, ca.hearingRange)
	ca.objects = env.objects
	ca.forbiddenWords = env.ForbiddenWords
	for agt, cooldown := range ca.cooldownCop {
		if cooldown > 0 {
			ca.cooldownCop[agt]--
		} else {
			delete(ca.cooldownCop, agt)
		}
	}
	// si un civil est detecté sur un mot interdit, il est mis dans la liste des suspects
	ca.sounds.Range(
		func(tempCoords, tempSound any) bool {
			coords := tempCoords.(Coordinates)
			sound := tempSound.(map[string]int)
			for word, timeout := range sound {
				if timeout > 0 && contains(ca.forbiddenWords, word) {
					if ag, exists := ca.world.Load(coords); exists {
						switch ag.(type) {
						case CivilianAgent:
							if !contains(ca.suspects, ag.(Agent).ID()) {
								ca.suspects = append(ca.suspects, ag.(Agent).ID())
							}
						}
					}
				}
			}
			return true
		})
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func removeElement[T Coordinates | string](s []T, e T) (res []T) {
	for _, coords := range s {
		if coords != e {
			res = append(res, coords)
		}
	}
	return
}

func getNeighboringCells(center Coordinates) (res []Coordinates) {
	res = make([]Coordinates, 0)
	for x := center[0] - 1; x <= center[0]+1; x++ {
		for y := center[1] - 1; y <= center[1]+1; y++ {
			if x > 0 && y > 0 { // can only detect lower edge of bounds
				coords := Coordinates{x, y}
				if coords != center {
					res = append(res, coords)
				}
			}
		}
	}
	return
}

func (ca *CopAgent) Deliberate() {
	/* ordre de priorité :
	1. interpeler les suspects
	2. se diriger vers un espace où se trouve des mots interdits
	3. partager sa liste de suspects aux autres policiers
	4. aléatoire ?
	*/
	emptyCells := getNeighboringCells(ca.position)
	var canInteractCop []Coordinates
	var canInteractCiv []Coordinates
	var seenCivilians []Coordinates
	var seenCops []Coordinates
	ca.world.Range(
		func(tempCoords, tempAgent any) bool {
			coords := tempCoords.(Coordinates)
			agent := tempAgent.(Agent)
			if coords != ca.position {
				switch agent.(type) {
				//Si elle contient un autre agent rebelle
				case CivilianAgent:
					if distance(ca.Coords(), agent.Coords()) < 2 {
						canInteractCiv = append(canInteractCiv, coords)
						emptyCells = removeElement(emptyCells, coords)
					} else {
						seenCivilians = append(seenCivilians, coords)
					}
				case *CopAgent:
					if distance(ca.Coords(), agent.Coords()) < 2 {
						canInteractCop = append(canInteractCop, coords)
						emptyCells = removeElement(emptyCells, coords)
					} else {
						seenCops = append(seenCops, coords)
					}
				}
			}
			return true
		})
	for _, coords := range canInteractCiv { // arrest civilian
		for _, suspect := range ca.suspects {
			ag, _ := ca.world.Load(coords)
			if ag.(CivilianAgent).ID() == suspect {
				ca.decision = Arrest
				ca.goalCoords = coords
				return
			}
		}
	}
	if len(emptyCells) > 0 {
		ca.sounds.Range(
			func(tempCoord, tempSound any) bool {
				coords := tempCoord.(Coordinates)
				sound := tempSound.(map[string]int)
				for word, timeLeft := range sound {
					if timeLeft > 0 {
						if contains(ca.forbiddenWords, word) {
							if newCoords, found := moveFromCoords(ca, emptyCells, coords, true); found {
								ca.decision = Move
								ca.goalCoords = newCoords
								return false // prend le premier qu'on trouve
							}
						}
					}
				}
				return true
			})
	}
	for _, coords := range canInteractCop { // share suspects to cop
		// si on ne lui a pas parlé récemment, on peut lui partager notre liste de suspects
		ag, _ := ca.world.Load(coords)
		if _, exists := ca.cooldownCop[ag.(*CopAgent)]; !exists {
			ca.decision = ShareSuspects
			ca.goalCoords = coords
			return
		}
	}
	if len(emptyCells) == 0 {
		ca.decision = Noop
		ca.goalCoords = ca.position
		return
	}
	if nbCivilians := len(seenCivilians); nbCivilians > 0 {
		if newCoords, found := moveFromCoords(ca, emptyCells, seenCivilians[rand.Intn(nbCivilians)], true); found {
			ca.decision = Move
			ca.goalCoords = newCoords
			return
		}
	}
	if nbCops := len(seenCops); nbCops > 0 {
		if newCoords, found := moveFromCoords(ca, emptyCells, seenCops[rand.Intn(nbCops)], true); found {
			ca.decision = Move
			ca.goalCoords = newCoords
			return
		}
	}
	if rand.Float64() < 0.1 {
		ca.decision = Noop
		ca.goalCoords = ca.position
		return
	} else {
		ca.decision = Move
		ca.goalCoords = emptyCells[rand.Intn(len(emptyCells))]
		return
	}
}

func (ca *CopAgent) Act(env *Env) {
	//simulation d'une action
	//time.Sleep(500 * time.Millisecond)
	err := env.Do(ca.decision, ca.goalCoords, ca)
	switch ca.decision {
	case Noop:
	case Move:
		if err == nil {
			ca.position = ca.goalCoords
			fmt.Printf("Agent {%s} -> (%d, %d) \n", ca.ID(), ca.position[0], ca.position[1])
		}
	case Arrest:
		if err == nil {
			suspect, _ := ca.world.Load(ca.goalCoords)
			ca.suspects = removeElement(ca.suspects, suspect.(Agent).ID())
			fmt.Printf("J'ai arrêté l'agent {%s} \n", suspect.(Agent).ID())
		}
	case ShareSuspects:
		if err == nil {
			otherAgent, _ := ca.world.Load(ca.goalCoords)
			fmt.Printf("J'ai partagé mes suspects avec {%s} \n", otherAgent.(Agent).ID())
			ca.cooldownCop[otherAgent.(*CopAgent)] = 10
			ca.suspects = make([]string, 0)
		}
	default:
		fmt.Printf("L'action n°%d n'est pas implémentée, je ne sais pas ce que j'ai fait", ca.decision)
	}
}
