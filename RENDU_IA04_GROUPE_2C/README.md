# Babel Revolution - IA04 Groupe 2C

## Description du projet

Dans une société dystopique un centre de la monoculture (CMC) bannit peu à peu l’utilisation des mots.

Une rébellion s’organise.

Les rebelles ont pour but de conserver le plus de mots possible alors que le CMC interdit petit à petit de plus en plus de mots.
Des pliciers sont déployés pour empêcher les rebelles de communiquer 
mais comme les rebelles s'en méfient et évitent de parler devant eux, 
des espions sont aussi envoyés sur le terrain. 
Ils pourront ensuite dénoncer les rebelles qu'ils trouveront.

Les rebelles ont un système de mémoire et doivent donc communiquer souvent 
pour se rappeler des mots qu'il connaissent.

Notre simulation modélise cette situation.

## Lancement de la simulation

 Lancer le serveur (fichier main.go) qui se met en attente d'une requête pour paramétrer et lancer la simulation.

Il faudra ensuite lancer le frontend associé avec `npm` ou directement envoyer des requêtes suivant les formats indiqués dans simu/server_side.go:

```
type CreationRequest struct {
	Height      int `json:"height"`
	Width       int `json:"width"`
	NbRebels    int `json:"nbRebels"`
	NbCops      int `json:"nbCops"`
	NbSpies     int `json:"nbspies"`
	MaxStep     int `json:"maxStep"`
	MaxDuration int `json:"maxDuration"`
}
```

### Images du projet

Des capturesd d'écran du projet peuvent être trouvées dans le dossier `doc`
