# BABEL REVOLUTION - Simulation IA04

### Présentation
Ce projet comprend la partie visualisation front-end de la simulation BABEL REVOLUTION - IA04. Pour rappel, il s'agit de la simulation d'un scenario dans lequel le Centre de la Monoculture décrète progressivement l'interdiction de certains mots de la langue française. Le scénario est dynamique et implique des rebelles, des policiers et des espions dans un environnement basé sur une grille. La simulation suit l'activité de discussion entre les "personnages" et le nombre de prisonniers au fil du temps, affichant les résultats à travers des graphiques et une visualisation de la grille. Le projet utilise ReactJs ainsi que des composants chartJs afin de gérer les graphiques.

### Fonctionnalités

- **Configuration de la Simulation :**
    La simulation est paramétrable :
    - Hauteur de la grille
    - Largeur de la grille
    - Nombre d'agents rebelles
    - Nombre d'agents policiers
    - Nombre d'agents espions
    Après validation, une requête est envoyée au serveur avec tous les paramètres saisis afin de démarrer la simulation.

- **Mises à Jour en Temps Réel :**
  - L'application communique avec un serveur backend pour effectuer des mises à jour en temps réel sur l'état de la simulation. Toutes les 750ms (pourrait être adaptée), une requête est envoyée à une API REST afin d'obtenir l'état actuel complet de la simulation.

- **Liste des mots :**
  - La liste des mots interdits évoluant continuellement, elle fait également partie de l'état de la simulation et la liste est visible et actualisée à l'écran.

- **Visualisation de la Grille :**
  - La grille visualise l'état actuel de la simulation avec la position des rebelles, policiers et espions.

- **Représentation Graphique :**
  - Deux graphiques représentent visuellement l'évolution de l'activité de communication entre les personnages et du nombre de prisonniers au fil du temps.

### Composants

- **SimulationGrid :**
  - Rend la visualisation de la grille de la simulation, affichant les agents sous forme d'emojis en fonction de leur type (Rebelles, Policiers, Espions).

- **SimulationWords :**
  - Affiche un tableau de mots interdits utilisés dans la simulation.

- **App :**
  - Le composant principal de l'application, il gère la configuration/lancement de la simulation, les mises à jour en temps réel et la visualisation.

### Démarrage du projet

- NodeJS doit être installé sur le système.
- Lancer le serveur `go run source/main.go`
- Lancer le front: Dans le dossier front
- `npm install` installe correctement les dépendances du projet
- `npm start` permet de démarrer le projet sur le port par défaut.

### Contributeurs

- Cette application a été développée par : Balthazar JARRY-WILSON, Edouard BLANC, Gabrielle VAN DE VIJVER, Benoit CHEVILLON