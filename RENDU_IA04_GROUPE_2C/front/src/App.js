import './App.css';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import SimulationGrid from './components/SimulationGrid';
import SimulationWords from './components/SimulationWords';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend } from 'chart.js';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

function App() {
  let simulationInterval = null;
  const [running, setRunning] = useState(false);

  // Paramètres par défaut
  const [formData, setFormData] = useState({
    height: 15,
    width: 15,
    nbRebels: 50,
    nbCops: 8,
    nbSpies: 5,
    maxStep: -1,
    maxDuration: 600,
  });

  // On initialise l'instant 0 de la simulation avec 0 mots encore connus, 0 mots prononcés et 0 prisonniers
  const [nbWordsWorld, setNbWordsWorld] = useState(0);
  const [nbWords, setNbWords] = useState({id: 0, nb: 0});
  const [nbPrisoners, setNbPrisoners] = useState({id: 0, nb: 0});
  
  // Par défaut, les graphiques sont vides
  const [dataLanguage, setDataLanguage] = useState({ 
    labels: [],
    datasets: [
      {
        label: 'Nombre de mots en usage',
        data: [],
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1,
      },
    ],
  });
  const [dataPrisoners, setDataPrisoners] = useState({
    labels: [],
    datasets: [
      {
        label: 'Nombre de prisonniers',
        data: [],
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1,
      },
    ],
  });
  const [dataGrid, setDataGrid] = useState([]);
  const [dataWords, setDataWords] = useState([]);

  // Options des graphiques
  const optionsLanguage = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      legend: {
        position: 'bottom',
      },
      title: {
        display: true,
        text: 'Communication',
      },
    },
  };
  const optionsPrisoners = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      legend: {
        position: 'bottom',
      },
      title: {
        display: true,
        text: 'Prison',
      },
    },
  };

  const handleInputChange = (e) => { // Gestion des saisis dans le formulaire
    const numericValue = parseFloat(e.target.value);
    setFormData({ ...formData, [e.target.name]: numericValue });
  };
  
  const handleSubmit = (e) => { // Validation du formulaire
    e.preventDefault();
    fetch('http://localhost:8080/new_simu', { // Envoi des paramètres de la simulation
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then((response) => {
        if (response.status === 200) {
          console.log('Simulation lancée');
          setRunning(true);
        }
      })
      .catch((error) => {
        console.error('Erreur :', error);
      });
  };

  // Si une simulation est déjà en cours d'execution, on la démarre dans une grille 30x30
  useEffect(() => {
    fetch('http://localhost:8080/update')
      .then((response) => {
        if (response.status === 200) {
          setFormData({...formData, height: 30, width: 30});
          setRunning(true);
        } else {
          console.log('Aucune simulation en cours');
        }
      });
  }, []);

  // Actualisation toutes les 750ms, le délai pourrait être adapté
  useEffect(() => {
    if (running) {
      simulationInterval = setInterval(() => {
        fetch('http://localhost:8080/update')
          .then((response) => {
            if (response.status === 200) {
              response = response.json()
              .then((response) => {
                setDataWords(response.forbiddenWords);
                setDataGrid({
                  world: response.world,
                  width: formData.width,
                  height: formData.height
                });
                setNbPrisoners({id: nbPrisoners.id++, nb: response.prison.length});
                setNbWords({id: nbWords.id++, nb: response.nb_mot_second});
                setNbWordsWorld(response.nb_word_vocab);
              });
            } else {
              clearInterval(simulationInterval);
              setRunning(false);
              console.log('Aucune simulation en cours');
            }
          })
        }, 750); 
    }
    else
      clearInterval(simulationInterval);
  }, [running]);

  // Actualisation des graphiques lorsque les données changent
  useEffect(() => {
    const newLanguage = {
      labels: [...dataLanguage.labels, nbWords.id],
      datasets: [
        {
          label: 'Nombre de mots prononcés',
          data: [...dataLanguage.datasets[0].data, nbWords.nb],
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0.1,
        },
      ],
    };
    setDataLanguage(newLanguage);
  }, [nbWords]);
  useEffect(() => {
    const newLanguage = {
      labels: [...dataPrisoners.labels, nbPrisoners.id],
      datasets: [
        {
          label: 'Nombre de prisonniers',
          data: [...dataPrisoners.datasets[0].data, nbPrisoners.nb],
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          tension: 0.1,
        },
      ],
  };
  setDataPrisoners(newLanguage);
}, [nbPrisoners]);

  if (!running) {
    return (
      <div className="launch">
        <h1>Configuration de la Simulation</h1>
        <form className="config-form" onSubmit={handleSubmit}>
          <label htmlFor="height">Height:</label>
          <input type="number" id="height" name="height" value={formData.height} onChange={handleInputChange} required min="0" /><br />
          <label htmlFor="width">Width:</label>
          <input type="number" id="width" name="width" value={formData.width} onChange={handleInputChange} required min="0" /><br />
          <label htmlFor="nbRebels">Number of Rebels:</label>
          <input type="number" id="nbRebels" name="nbRebels" value={formData.nbRebels} onChange={handleInputChange} required min="0" /><br />
          <label htmlFor="nbCops">Number of Cops:</label>
          <input type="number" id="nbCops" name="nbCops" value={formData.nbCops} onChange={handleInputChange} required min="0" /><br />
          <label htmlFor="nbSpies">Number of Spies:</label>
          <input type="number" id="nbSpies" name="nbSpies" value={formData.nbSpies} onChange={handleInputChange} required min="0" /><br />
          <label htmlFor="maxStep">Max Step:</label>
          <input type="number" id="maxStep" name="maxStep" value={formData.maxStep} onChange={handleInputChange} required min="-1" /><br />
          <label htmlFor="maxDuration">Max Duration:</label>
          <input type="number" id="maxDuration" name="maxDuration" value={formData.maxDuration} onChange={handleInputChange} required min="0" /><br />          
          <input type="submit" value="Start Simulation" />
        </form>
      </div>
    );
  } else {
    return (
      <div className="App">
        <h1>Simulation BABEL REVOLUTION - IA04</h1>
        <div className="simulation">
          <div className="words">
            <SimulationWords data={dataWords} />
          </div>
          <div>
            <SimulationGrid data={dataGrid} />
          </div>
          <div style={{ width: '500px' }}></div>
        </div>
        <div>
          <p style={{textAlign: "center"}}>Nombre de mots encore connus dans le monde : <span style={{fontWeight: "bold"}}>{nbWordsWorld}</span> (vocabulaire global) </p>
        </div>
        <div className="charts">
          <Line options={optionsLanguage} data={dataLanguage} />
          <Line options={optionsPrisoners} data={dataPrisoners} />
        </div>
      </div>
    );
  }
}

export default App;