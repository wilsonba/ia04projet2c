import { useEffect, useState } from 'react';

function SimulationWords(props) {
    const [wordsTable, setWordsTable] = useState([]);
    
    useEffect(() => {
        if (props.data === undefined) return; // Si aucune donnée, aucun affichage

        const table = props.data.map((word, index) => ( // Pour chaque mot, on ajoute une ligne
            <tr key={index}>
                <td>{word}</td>
            </tr>
        ));

        setWordsTable(table);
    }, [props.data]);

    return (
        <table>
            <thead>
                <tr>
                    <td style={{ fontSize: "1.2em", width: "max-content" }}>Mots interdits</td>
                </tr>
            </thead>
            <tbody>{wordsTable}</tbody>
        </table>
    );
}

export default SimulationWords;
