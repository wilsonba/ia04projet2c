import React from 'react';

function SimulationGrid(props) {
  const generateGrid = () => {
    if (!props.data || !props.data.world) return null; // Si aucune donnée, aucun affichage

    const world = props.data.world;

    const rows = [];
    // Ajout des agents dans la grille
    for (let i = 0; i < props.data.height; i++) {
      const cells = [];
      for (let j = 0; j < props.data.width; j++) {
        const coordinates = `${i},${j}`;
        let cellContent = world[coordinates] || '';
        let classCell = "";
        if (cellContent.startsWith('RbAgt')) { 
          cellContent = '👨‍🎤';
        } else if (cellContent.startsWith('cop')) {
          cellContent = '👮';
          classCell = "cop" // Classe spécifique pour couleur spécifique
        } else if (cellContent.startsWith('SpyAgt')) {
          cellContent = '🕵️';
        }

        cells.push(<td key={coordinates} className={classCell}>{cellContent}</td>);
      }
      rows.push(<tr key={i}>{cells}</tr>);
    }

    return (
      <div className="simulationGrid">
        <table>
          <tbody>{rows}</tbody>
        </table>
      </div>
    );
  };

  return generateGrid();
}

export default SimulationGrid;
